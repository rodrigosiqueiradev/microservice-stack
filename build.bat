mvn -f ./auth-server/pom.xml install dockerfile:build -DskipTests
mvn -f ./eureka-server/pom.xml install dockerfile:build -DskipTests
mvn -f ./gateway-server/pom.xml install dockerfile:build -DskipTests
mvn -f ./sample1-server/pom.xml install dockerfile:build -DskipTests
mvn -f ./sample2-server/pom.xml install dockerfile:build -DskipTests

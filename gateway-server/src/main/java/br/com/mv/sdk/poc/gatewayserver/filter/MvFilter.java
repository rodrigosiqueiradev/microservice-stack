package br.com.mv.sdk.poc.gatewayserver.filter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.discovery.EurekaClient;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class MvFilter extends ZuulFilter {

    @Autowired
    EurekaClient discoveryClient;

    private static Logger log = LoggerFactory.getLogger(MvFilter.class);

    @Override
    public boolean shouldFilter() {
	return true;
    }

    @Override
    public String filterType() {
	return "pre";
    }

    @Override
    public int filterOrder() {
	return 6;
    }

    @Override
    public Object run() throws ZuulException {
	RequestContext ctx = RequestContext.getCurrentContext();
	HttpServletRequest request = ctx.getRequest();
	log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL()
		.toString()));
	return null;
    }

}

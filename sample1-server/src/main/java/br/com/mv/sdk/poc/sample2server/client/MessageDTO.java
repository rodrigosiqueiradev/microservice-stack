package br.com.mv.sdk.poc.sample2server.client;

import java.io.Serializable;

public class MessageDTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String name;
	Long age;
	Long timestamp = System.currentTimeMillis();
	public MessageDTO(String name, Long age) {
		this.name = name;
		this.age = age;
	}	
}

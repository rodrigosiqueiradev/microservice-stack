package br.com.mv.sdk.poc.sample1server.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SampleQueueSender {
	Logger LOGGER = LoggerFactory.getLogger(SampleQueueSender.class);

	@Autowired
	RabbitTemplate rabbitTemplate;

	@Autowired
	@Qualifier("Sample1Queue")
	Queue sample1Queue;

	@Autowired
	@Qualifier("Sample2Queue")
	Queue sample2Queue;

	@Autowired
	Environment env;

	@Scheduled(fixedDelay = 200, initialDelay = 500)
	public void sendToSample2() {
		try {
			LOGGER.info(((br.com.mv.sdk.poc.sample2server.client.MessageDTO) rabbitTemplate
					.convertSendAndReceive(sample2Queue.getName(), "Hello")).name);
		} catch (Exception e) {
			LOGGER.error("No response from RPC-Sample2");
		}
	}

	@RabbitListener(queues = { "${queue.sample1.name}" })
	public MessageDTO receive(@Payload String msg) {
		return new MessageDTO(env.getProperty("server.port"), 1L);
	}
}

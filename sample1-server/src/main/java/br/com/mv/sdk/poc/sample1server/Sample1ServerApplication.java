package br.com.mv.sdk.poc.sample1server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import brave.sampler.Sampler;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class Sample1ServerApplication {

    @Autowired
    public RestTemplate template;

    @Bean
    public RestTemplate template() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
	    SpringApplication.run(Sample1ServerApplication.class, args);
    }

    @Bean
    public Sampler defaultSampler() {
	    return Sampler.ALWAYS_SAMPLE;
    }

}

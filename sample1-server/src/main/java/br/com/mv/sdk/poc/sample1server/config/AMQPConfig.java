package br.com.mv.sdk.poc.sample1server.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableRabbit
@EnableScheduling
public class AMQPConfig {

	@Value("${queue.sample1.name}")
	String queueSample1;
	
	@Value("${queue.sample2.name}")
	String queueSample2;

	@Bean("Sample1Queue")
	public Queue queue1() {
		return new Queue(queueSample1, true);
	}
	
	@Bean("Sample2Queue")
	public Queue queue2() {
		return new Queue(queueSample2, true);
	}

}

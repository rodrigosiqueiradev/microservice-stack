package br.com.mv.sdk.poc.sample1server.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.util.Assert;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class OAuth2FeignRequestInterceptor implements RequestInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2FeignRequestInterceptor.class);

    private final OAuth2ClientContext oauth2ClientContext;

    public OAuth2FeignRequestInterceptor(OAuth2ClientContext oAuth2ClientContext) {
	Assert.notNull(oAuth2ClientContext, "Context can not be null");
	this.oauth2ClientContext = oAuth2ClientContext;
    }

    @Override
    public void apply(RequestTemplate template) {
	if (template.headers()
		.containsKey("Authorization")) {
	    LOGGER.warn("The Authorization token has been already set");
	} else if (oauth2ClientContext.getAccessTokenRequest()
		.getExistingToken() == null) {
	    LOGGER.warn("Can not obtain existing token for request, if it is a non secured request, ignore.");
	} else {
	    LOGGER.debug("Constructing Header {} for Token {}", "Authorization", "Bearer");
	    template.header("Authorization",
		    String.format("%s %s", "Bearer", oauth2ClientContext.getAccessTokenRequest()
			    .getExistingToken()
			    .toString()));
	}

    }

}

package br.com.mv.sdk.poc.sample1server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mv.sdk.poc.sample1server.client.Sample2Client;
import br.com.mv.sdk.poc.sample1server.client.SampleQueueSender;

@RestController
@RequestMapping("/")
public class SampleController {

	final Logger LOGGER = LoggerFactory.getLogger(SampleController.class);

	@Autowired
	Sample2Client sample2;

	@Autowired
	Environment env;

	@Autowired
	SampleQueueSender sender;

	@GetMapping("/hello")
	public String sayHello() {
		OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();

		LOGGER.info("Returning hello from service1");
		return "Hello " + auth.getOAuth2Request().getClientId() + " from service 1 running at port: "
				+ env.getProperty("local.server.port");
	}

	@GetMapping("/hello-from-2")
	public String talkWithSample2() {
		LOGGER.info("Requesting hello from service2");
		return sample2.hello();
	}

}
package br.com.mv.sdk.poc.sample2server.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/* 
 * --------------------------------------------------------------------------
 * THIS INTERFACE SHOULD BE CREATED BY RESPECTIVE SERVICE IMPLEMENTER
 * AND MADE AVAILABLE IN SELF-CONTAINED JAR LIB
 * --------------------------------------------------------------------------
 */
@FeignClient(name="sample-service-1")
public interface Sample1Client {
    
    @RequestMapping(method=RequestMethod.GET, value="/hello")
    String hello();
}

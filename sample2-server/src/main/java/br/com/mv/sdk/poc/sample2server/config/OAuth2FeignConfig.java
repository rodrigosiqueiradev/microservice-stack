package br.com.mv.sdk.poc.sample2server.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;

import br.com.mv.sdk.poc.sample2server.interceptor.OAuth2FeignRequestInterceptor;
import feign.Feign;
import feign.RequestInterceptor;

@Configuration
@ConditionalOnClass({ Feign.class })
@ConditionalOnProperty(value = "feign.oauth2.enabled", matchIfMissing = true)
public class OAuth2FeignConfig {

    @Bean
    @ConditionalOnBean({OAuth2ClientContext.class})
    public RequestInterceptor requestInterceptor(OAuth2ClientContext context) {
	return new OAuth2FeignRequestInterceptor(context);
    }

}

package br.com.mv.sdk.poc.sample2server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.client.RestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class Sample2ServerApplication {

    public static void main(String[] args) {
	    SpringApplication.run(Sample2ServerApplication.class, args);
    }

    @Autowired
    public RestTemplate template;

    @Bean
    public RestTemplate template() {
        return new RestTemplate();
    }
}

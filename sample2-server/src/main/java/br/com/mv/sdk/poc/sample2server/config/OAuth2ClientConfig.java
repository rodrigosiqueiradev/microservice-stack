package br.com.mv.sdk.poc.sample2server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

@Configuration
@EnableOAuth2Client
public class OAuth2ClientConfig {

    @Bean
    protected ClientCredentialsResourceDetails oAuthDetails() {
	return new ClientCredentialsResourceDetails();
    }

}

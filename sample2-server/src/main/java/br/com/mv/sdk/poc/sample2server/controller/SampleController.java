package br.com.mv.sdk.poc.sample2server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mv.sdk.poc.sample2server.client.Sample1Client;

@RestController
@RequestMapping("/")
public class SampleController {

    final Logger LOGGER = LoggerFactory.getLogger(SampleController.class);
    
    @Autowired
    Sample1Client sample1;

    @Autowired
    Environment env;

    @GetMapping("/hello")
    public String sayHello() {
	OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext()
		.getAuthentication();

	LOGGER.info("Returning hello from service2");
	return "Hello " + auth.getOAuth2Request()
		.getClientId() + " from service 2 running at port: " + env.getProperty("local.server.port");
    }

    @GetMapping("/hello-from-1")
    public String talkWithSample1() {
	LOGGER.info("Requesting hello from service2");
	return sample1.hello();
    }
    
    @GetMapping("/fail")
    public ResponseEntity<String> fail() {
	LOGGER.info("Failing hello from service2");
	return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }
}
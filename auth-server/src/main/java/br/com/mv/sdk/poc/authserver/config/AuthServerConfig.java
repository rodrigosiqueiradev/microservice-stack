package br.com.mv.sdk.poc.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
	JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
	converter.setVerifierKey("123");
	converter.setSigningKey("123");
	return converter;
    }

    @Bean
    public TokenStore tokenStore() {
	return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
	security.checkTokenAccess("hasAuthority('CLIENT')")
		.tokenKeyAccess("hasAuthority('CLIENT')");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
	endpoints.authenticationManager(authenticationManager)
		.accessTokenConverter(jwtAccessTokenConverter())
		.tokenStore(tokenStore());
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
	clients.inMemory()
		.withClient("gateway")
		.secret(encoder.encode("secret"))
		.authorizedGrantTypes("client_credentials")
		.authorities("CLIENT")
		.scopes("mv")
		.and()
		.withClient("sample1")
		.secret(encoder.encode("secret"))
		.authorizedGrantTypes("client_credentials")
		.authorities("CLIENT")
		.scopes("mv")
		.and()
		.withClient("sample2")
		.secret(encoder.encode("secret"))
		.authorizedGrantTypes("client_credentials")
		.authorities("CLIENT")
		.scopes("mv")
		.and()
		.withClient("third")
		.secret(encoder.encode("secret"))
		.authorizedGrantTypes("client_credentials")
		.authorities("CLIENT")
		.scopes("not-mv");
    }

}
